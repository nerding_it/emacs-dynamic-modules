use emacs::{defun, Env, Result};
use sys_info::{os_type, os_release, proc_total, hostname, cpu_num, cpu_speed };

emacs::plugin_is_GPL_compatible!();

#[emacs::module(name = "sysinfo")]
fn init(_: &Env) -> Result<()> { Ok(()) }

#[defun]
fn get_os_type() -> Result<String> {
    Ok(os_type().unwrap())
}

#[defun]
fn get_os_release() -> Result<String> {
    Ok(os_release().unwrap())
}

#[defun]
fn get_proc_total() -> Result<u64> {
    Ok(proc_total().unwrap())
}

#[defun]
fn get_hostname() -> Result<String> {
    Ok(hostname().unwrap())
}

#[defun]
fn get_cpu_num() -> Result<u32> {
    Ok(cpu_num().unwrap())
}

#[defun]
fn get_cpu_speed() -> Result<u64> {
    Ok(cpu_speed().unwrap())
}
