* Simple Emacs dynamic module wrapper for [[https://docs.rs/sys-info/0.9.0/sys_info/][sys_info]] crate

* Prerequisite
  - [[https://doc.rust-lang.org/book/ch01-03-hello-cargo.html][Cargo]]
  - [[https://github.com/ubolonton/emacs-module-rs][emacs-rs-module]]

* Configuration
  - We need a function ~rs-module/load~ for reloading the modules, You can use built-in ~module-load~ also but it doesn't work with reloading
    So we need to load ~emacs-rs-module~, Use below snippet to that please change it to right path
    #+BEGIN_SRC emacs-lisp
      (module-load (expand-file-name "libemacs_rs_module.dylib" "~/.cargo/registry/src/github.com-1ecc6299db9ec823/emacs-rs-module-0.17.0/target/debug"))    
    #+END_SRC
    Now we have ~rs-module/load~ function in place

* Building
  Use ~cargo build~ to build the project, After building the project you can find ~target/debug~ directory and you'll see the file ~libsys_info.dylib~. Navigate to that directory from ~eshell~ or ~dired~ then use below code to load the module
  #+NAME: 
  #+BEGIN_SRC emacs-lisp
    (rs-module/load (expand-file-name "libsys_info.dylib"))
  #+END_SRC

* Usage
  This module exports below functions, So just call as ~elisp~ function to test it out
  - ~get_os_type~
  - ~get_os_release~
  - ~get_proc_total~
  - ~get_hostname~
  - ~get_cpu_num~
  - ~get_cpu_speed~

    
