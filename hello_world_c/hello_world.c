#include "emacs-module.h"

int plugin_is_GPL_compatible;

int
emacs_module_init(struct emacs_runtime *ert)
{
    emacs_env *env = ert->get_environment(ert);
    emacs_value switch_to_buffer = env->intern(env, "switch-to-buffer");
    emacs_value insert_fn = env->intern(env, "insert");
    const char hello_world[] = "Hello World!";
    const char buffer_name[] = "*scratch*";
    emacs_value string = env->make_string(env, hello_world, sizeof(hello_world) - 1);
    emacs_value buffer = env->make_string(env, buffer_name, sizeof(buffer_name) - 1);
    env->funcall(env, switch_to_buffer, 1, &buffer);
    env->funcall(env, insert_fn, 1, &string);
    return 0;
}
